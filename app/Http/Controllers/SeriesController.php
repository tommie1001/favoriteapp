<?php

namespace App\Http\Controllers;

use App\Movies;
use App\Series;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SeriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $series=DB::table('series')->get();
        return view('series.index',compact('series'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('series.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('series')->insert([
            'title'=> $request['title'],
            'status'=> $request['status'],
            'genre'=> $request['genre'],
        ]);
        return redirect()->route('series');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movies  $movies
     * @return \Illuminate\Http\Response
     */
    public function show(Movies $movies)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $data=  DB::table('series')->where('id','=',$request['id'])->get();
        return view('series.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movies  $movies
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('series')->where('id','=',$request['id'])->update([
            'title'=> $request['title'],
            'status'=> $request['status'],
            'genre'=> $request['genre'],
        ]);
        return redirect()->route('series');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('series')->where('id','=',$request['id'])->delete();
        return redirect()->route('series');
    }
}
