<?php

namespace App\Http\Controllers;

use App\Movies;
use App\Workitems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WorkitemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workitems=DB::table('workitems')->get();
        return view('workitems.index',compact('workitems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('workitems.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        DB::table('workitems')->insert([
            'title'=> $request['title'],
            'description'=> $request['description'],
            'status' => $request['status'],
        ]);
        return redirect()->route('workitems');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movies  $movies
     * @return \Illuminate\Http\Response
     */
    public function show(Movies $movies)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $data=  DB::table('workitems')->where('id','=',$request['id'])->get();
        return view('workitems.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movies  $movies
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('workitems')->where('id','=',$request['id'])->update([
            'title'=> $request['title'],
            'description'=> $request['description'],
            'status' => $request['status'],
        ]);
        return redirect()->route('workitems');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('workitems')->where('id','=',$request['id'])->delete();
        return redirect()->route('workitems');
    }
}
