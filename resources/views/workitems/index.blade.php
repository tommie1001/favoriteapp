@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Workitems</li>
        </ol>
    </nav>
    <div id="container" style="width:80vw; height:100vh; border-radius:10px; margin:0 auto; background-color: lightblue;">
        <div style="width:100%; height:50px; background-color:cadetblue; border-radius:10px 10px 0 0;">
            <h1 style="font-size:25px; height: 40px; width:200px; float:left; padding:13px 0 0 5px;">Add Workitem</h1>
            <a href="/addworkitem" class="btn btn-success" style="float:left; margin-top:7px;">+</a>
        </div>
        @foreach($workitems as $workitem)
            <div class="card" style="width: 20rem; float:right; margin:10px;">
                <div class="card-body">
                    <h5 class="card-title">Title: {{$workitem->title}}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Genre:  {{$workitem->description}}</h6>
                    <p class="card-text">Status: {{$workitem->status}}</p>
                    <form action="{{action('WorkitemsController@destroy')}}" method="post" style="width:50%; float:left;">
                        @csrf
                        <input type="hidden" name="id" value="{{$workitem->id}}">
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                    <form action="{{action('WorkitemsController@edit')}}" style="width:50%; float:right;" method="post">
                        @csrf
                        <input type="hidden" value="{{$workitem->id}}" name="id">
                        <button type="submit" class="btn btn-warning">Edit</button>
                    </form>
                </div>
            </div>
        @endforeach
    </div>
@endsection
