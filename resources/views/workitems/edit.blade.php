@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home">Home</a></li>
            <li class="breadcrumb-item"><a href="/workitems">Workitems</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit Workitem</li>
        </ol>
    </nav>
    @foreach($data as $item)
        <div id="container" style="width:20em; margin:0 auto;">
            <form action="{{action('WorkitemsController@update')}}" method="post">
                @csrf
                <input type="hidden" value="{{$item->id}}" name="id" >
                <div class="form-group">
                    <label for="exampleFormControlInput1">name</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="{{$item->title}}" name="title">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Description</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="{{$item->description}}" name="description">
                </div>
                <div class="form-group">
                    <label for="status">status</label>
                    <select class="form-control" id="status" name="status">
                        <option>To Do</option>
                        <option>Doing</option>
                        <option>Done</option>
                    </select>
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-primary">
                </div>
            </form>
            @endforeach
        </div>
@endsection

