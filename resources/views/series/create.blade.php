@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home">Home</a></li>
            <li class="breadcrumb-item"><a href="/series">series</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add Serie</li>
        </ol>
    </nav>
    <div id="container" style="width:20em; margin:0 auto;">
        <form action="{{action('SeriesController@store')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="exampleFormControlInput1">name</label>
                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="title" name="title">
            </div>
            <div class="form-group">
                <label for="genre">genre</label>
                <select class="form-control" id="genre" name="genre">
                    <option>Action</option>
                    <option>Adventure</option>
                    <option>Animation</option>
                    <option>Biography</option>
                    <option>Comedy</option>
                    <option>Crime</option>
                    <option>Documentary</option>
                    <option>Drama</option>
                    <option>Family</option>
                    <option>Fantasy</option>
                    <option>Film Noir</option>
                    <option>History</option>
                    <option>Horror</option>
                    <option>Music</option>
                    <option>Musical</option>
                    <option>Mystery</option>
                    <option>Romance</option>
                    <option>Sci-Fi</option>
                    <option>Short Film</option>
                    <option>Sport</option>
                    <option>Superhero</option>
                    <option>Thriller</option>
                    <option>War</option>
                    <option>Western</option>
                </select>
            </div>
            <div class="form-group">
                <label for="status">status</label>
                <select class="form-control" id="status" name="status">
                    <option>not seen</option>
                    <option>seen</option>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary">
            </div>
        </form>
    </div>
@endsection

