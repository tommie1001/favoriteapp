@extends('layouts.app')
@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home">Home</a></li>
            <li class="breadcrumb-item"><a href="/books">Books</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add Books</li>
        </ol>
    </nav>
    <div id="container" style="width:20em; margin:0 auto;">
        <form action="{{action('BooksController@store')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="exampleFormControlInput1">name</label>
                <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="title" name="title">
            </div>
            <div class="form-group">
                <label for="genre">genre</label>
                <select class="form-control" id="genre" name="genre">
                    <option>Action and adventure</option>
                    <option>Alternate history</option>
                    <option>Anthology</option>
                    <option>Chick lit</option>
                    <option>Children's</option>
                    <option>Comic book</option>
                    <option>Coming-of-age</option>
                    <option>Crime</option>
                    <option>Drama</option>
                    <option>Fairytale</option>
                    <option>Fantasy</option>
                    <option>Graphic novel</option>
                    <option>Historical fiction</option>
                    <option>Paranormal romance</option>
                    <option>Picture book</option>
                    <option>Poetry</option>
                    <option>Political thriller</option>
                    <option>Romance</option>
                    <option>Satire</option>
                    <option>Science fiction</option>
                    <option>Short story</option>
                    <option>Suspense</option>
                    <option>Thriller</option>
                    <option>Young adult</option>
                </select>
            </div>
            <div class="form-group">
                <label for="type">type book</label>
                <select class="form-control" id="type" name="type">
                    <option>Normal Book</option>
                    <option>E-Book</option>
                </select>
            </div>
            <div class="form-group">
                <label for="status">status</label>
                <select class="form-control" id="status" name="status">
                    <option>not read</option>
                    <option>read</option>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary">
            </div>
        </form>
    </div>
@endsection

