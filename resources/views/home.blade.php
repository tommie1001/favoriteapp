@extends('layouts.app')

@section('content')
<div class="container">
   <div id="info">
      <a href="/books" class="btn btn-primary">Books</a>
       <a href="/movies" class="btn btn-primary">Movies</a>
       <a href="/series" class="btn btn-primary">Series</a>
       <a href="/workitems" class="btn btn-primary">Workitems</a>
       <h1 style="margin:10% 0 0 0;">
           About the app
       </h1>
       <p>
           Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur molestias nisi pariatur sed? Accusantium architecto, aspernatur autem cumque deleniti, doloremque eius expedita id in ipsam labore nam necessitatibus perferendis provident quia quibusdam quis quod sit temporibus vel, voluptas voluptatem voluptatum. Eos magnam minus modi veniam veritatis! Aperiam architecto autem culpa deserunt eius error est impedit iste maiores maxime nisi officia optio perferendis placeat porro praesentium quae quam reiciendis repellendus saepe, temporibus veniam voluptatum. Ab, adipisci alias architecto atque consequatur cum dolorem eligendi eos est ex explicabo, facere fugit id illo iste iure iusto magni modi molestiae nemo nulla placeat quae ratione reiciendis repellendus rerum similique tempora ullam veritatis voluptatem. Accusamus ad dolores maiores minima? Aspernatur assumenda beatae blanditiis commodi delectus distinctio eaque eos excepturi facilis fuga fugiat harum ipsam laboriosam magni maxime nam odit omnis pariatur placeat praesentium provident quas quasi quibusdam, quidem quo, quod repellendus repudiandae similique sit tempora temporibus veniam voluptas voluptate! Incidunt itaque libero omnis quos sed. Aliquam assumenda aut consequatur, dolore eveniet id illum natus, neque nihil perferendis sapiente sed, sequi tempore tenetur vero? Ab aliquam asperiores cumque distinctio doloribus ducimus earum, et fuga libero, officiis perspiciatis quas quasi reprehenderit saepe sapiente sed similique veritatis vero.
       </p>
   </div>
</div>
@endsection
