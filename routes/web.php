<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/books', 'BooksController@index')->name('books');
Route::get('/addbook', 'BooksController@create')->name('createbook');
Route::post('/postbooks', 'BooksController@store')->name('savebook');
Route::post('/editbooks', 'BooksController@edit')->name('edit');
Route::post('/updatebooks', 'BooksController@update')->name('update');
Route::post('/deletebook', 'BooksController@destroy')->name('destroy');

Route::get('/movies', 'MoviesController@index')->name('movies');
Route::get('/addmovie', 'MoviesController@create')->name('createmovie');
Route::post('/postmovie', 'MoviesController@store')->name('savemovie');
Route::post('/editmovie', 'MoviesController@edit')->name('editmovie');
Route::post('/updatemovie', 'MoviesController@update')->name('updatemovie');
Route::post('/deletemovie', 'MoviesController@destroy')->name('destroymovie');

Route::get('/series', 'SeriesController@index')->name('series');
Route::get('/addserie', 'SeriesController@create')->name('createserie');
Route::post('/postserie', 'SeriesController@store')->name('saveserie');
Route::post('/editserie', 'SeriesController@edit')->name('editserie');
Route::post('/updateserie', 'SeriesController@update')->name('updateserie');
Route::post('/deleteserie', 'SeriesController@destroy')->name('destroyserie');

Route::get('/workitems', 'WorkitemsController@index')->name('workitems');
Route::get('/addworkitem', 'WorkitemsController@create')->name('createworkitem');
Route::post('/postworkitem', 'WorkitemsController@store')->name('saveworkitem');
Route::post('/editworkitem', 'WorkitemsController@edit')->name('editworkitem');
Route::post('/updateworkitem', 'WorkitemsController@update')->name('updateworkitem');
Route::post('/deleteworkitem', 'WorkitemsController@destroy')->name('destroyworkitem');


